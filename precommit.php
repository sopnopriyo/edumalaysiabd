<?php
/* 
 * Author : Romeo San Jose Jr.
 */
echo PHP_EOL;

//set output file directory
$projectDir = dirname(__FILE__);
$gitPreCommitLogPath = $projectDir . "/.git/precommit/logs/";

// output a little introduction
echo '>> Starting unit tests' . PHP_EOL;

// get the name for this project; probably the topmost folder name
$projectName = basename(getcwd());

// execute unit tests (it is assumed that a phpunit.xml configuration is present 
// in the root of the project)
$result = exec('phpunit', $output, $returnCode); // cwd is assumed here

// if the build failed, output a summary and fail
if ($returnCode !== 0) {

//writes log file in .git/precommit/logs/
    if (!file_exists($gitPreCommitLogPath)){
        mkdir($gitPreCommitLogPath,0777,true);        
    }else{
        $gitPreCommitLogFile = $gitPreCommitLogPath . "precommit-" . time() . "log";
        if (!file_exists($gitPreCommitLogFile)){
            $fh = fopen($gitPreCommitLogFile, 'w');
            foreach ($output as $res){
                fwrite($fh, $res."\n");
            }
            fclose($fh);
        }
    }   
    
    // find the line with the summary; this might not be the last
    while (($minimalTestSummary = array_pop($output)) !== null) {
        if (strpos($minimalTestSummary, 'Tests:') !== false) {
            break;
        }
    }

    // output the status
    echo '>> Test suite for ' . $projectName . ' failed:' . PHP_EOL;
    echo $minimalTestSummary;
    echo chr(27) . '[0m' . PHP_EOL; // disable colors and add a line break
    echo PHP_EOL;

}