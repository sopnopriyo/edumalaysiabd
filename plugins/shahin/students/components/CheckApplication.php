<?php namespace Shahin\Students\Components;

use Cms\Classes\ComponentBase;
use Shahin\Students\Models\Services;
class CheckApplication extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'CheckApplication Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onTest()
    {
        $value1 = post('value');
        $post = Services::where('passport', '=', $value1)->get();
        $this->page['lastValue'] = $post ;

    }

  

}