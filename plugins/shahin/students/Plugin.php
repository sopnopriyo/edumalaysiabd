<?php namespace Shahin\Students;

use System\Classes\PluginBase;
use Backend;
/**
 * Students Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Students',
            'description' => 'No description provided yet...',
            'author'      => 'Shahin',
            'icon'        => 'icon-leaf'
        ];
    }
    public function registerNavigation()
    {
        return [
            'Students' => [
                'label'       => 'Students',
                'url'         => Backend::url('shahin/students/services'),
                'icon'        => 'icon-trophy',
                'permissions' => ['shahin.students.*'],
                'order'       => 100,
            ]
        ];
    }

     public function registerComponents()
    {
        return [
            'Shahin\Students\Components\CheckApplication' => 'CheckApplication'
        ];
    }

     public function registerPermissions()
    {
      return [
        'shahin.students.*'       => ['tab' => 'Students', 'order' => 100, 'label' => 'Manage Students']
        
        ];
    }


}
