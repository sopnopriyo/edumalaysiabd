/*------------------------------
 * Copyright 2015 Pixelized
 * http://www.pixelized.cz
 *
 * Progaming theme v1.0
------------------------------*/

$(document).ready(function() {	
     
        
	//NAVBAR
	$('.navbar .nav > li.dropdown').mouseenter(function() {
		$(this).addClass('open');
	});
	
	$('.navbar .nav > li.dropdown').mouseleave(function() {
		$(this).removeClass('open');
	});
	
	$('.countdown').countdown('2016/10/10', function(event) {
	    var $this = $(this).html(event.strftime(''
	      + '<div><span class="countdown-number">%w</span> <span class="countdown-title">weeks</span></div> '
	      + '<div><span class="countdown-number">%d</span> <span class="countdown-title">days</span></div> '
	      + '<div><span class="countdown-number">%H</span> <span class="countdown-title">hours</span></div> '
	      + '<div><span class="countdown-number">%M</span> <span class="countdown-title">minutes</span></div> '
	      + '<div><span class="countdown-number">%S</span> <span class="countdown-title">seconds</span></div>'));
	});
	
	//VEGAS
	//$.vegas({src:'assets/images/background-1.jpg'})('overlay', {src:'assets/images/overlay.png'});
	
	$.vegas('slideshow', {
		delay:5000,
		backgrounds:[
			{ src: back1 , fade:1500 }
			
			
	  	]
	})('overlay', {src:overlay});
	$('.gallery-page').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'div a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
	}); 

});



