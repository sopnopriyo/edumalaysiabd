<?php namespace Shahin\Pages;

use Event;
use Backend;
use Shahin\Pages\Classes\Controller;
use Shahin\Pages\Classes\Page as StaticPage;

use Cms\Classes\Theme;
use Cms\Classes\Controller as CmsController;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Menus',
            'description' => 'This is Dynamic Menu manager',
            'author'      => 'Gan Chun How, Shahin and Romeo',
            'icon'        => 'icon-files-o',
            'homepage'    => ''
        ];
    }

    public function registerComponents()
    {
        return [
            
            '\Shahin\Pages\Components\StaticMenu' => 'staticMenu'
          
        ];
    }

    public function registerPermissions()
    {
        return [
            'shahin.pages.manage_menus'    => ['tab' => 'shahin.pages::lang.page.tab', 'order' => 200, 'label' => 'Manage static menus']
                  ];
    }

    public function registerNavigation()
    {
        return [
            'pages' => [
                'label'       => 'Menu',
                'url'         => Backend::url('shahin/pages'),
                'icon'        => 'icon-files-o',
                'permissions' => ['shahin.pages.*'],
                'order'       => 20,

                'sideMenu' => [
                   
                    'menus' => [
                        'label'       => 'Menus',
                        'icon'        => 'icon-sitemap',
                        'url'         => 'javascript:;',
                        'attributes'  => ['data-menu-item'=>'menus'],
                        'permissions' => ['shahin.pages.manage_menus']
                    ]
                    
                ]

            ]
        ];
    }

    
}
