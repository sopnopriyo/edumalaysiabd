<?php namespace Shahin\Pages\Controllers;

use URL;
use Lang;
use Flash;
use Event;
use Config;
use Request;
use Response;
use Exception;
use BackendMenu;
use ApplicationException;
use Shahin\Pages\Widgets\MenuList;
use Shahin\Pages\Classes\Router;
use Shahin\Pages\Classes\MenuItem;
use Shahin\Pages\Plugin as PagesPlugin;
use Backend\Classes\Controller;
use Backend\Classes\WidgetManager;
use Cms\Classes\Theme;
use Cms\Classes\Content;
use Cms\Widgets\TemplateList;

/**
 * Pages and Menus index
 *
 * @package rainlab\pages
 * @author Alexey Bobkov, Samuel Georges
 */
class Index extends Controller
{
    use \Backend\Traits\InspectableContainer;

    protected $theme;

    public $requiredPermissions = ['shahin.pages.*'];

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Shahin.Pages', 'pages', 'pages');

        try {
            if (!($this->theme = Theme::getEditTheme())) {
                throw new ApplicationException(Lang::get('cms::lang.theme.edit.not_found'));
            }

            new MenuList($this, 'menuList');
           
            $theme = $this->theme;
            new TemplateList($this, 'contentList', function() use ($theme) {
                return Content::listInTheme($theme, true);
            });
        }
        catch (Exception $ex) {
            $this->handleError($ex);
        }
    }

    //
    // Pages, menus and text blocks
    //

    public function index()
    {
        $this->addJs('/modules/backend/assets/js/october.treeview.js', 'core');
        $this->addJs('/plugins/shahin/pages/assets/js/pages-page.js');
        $this->addJs('/plugins/shahin/pages/assets/js/pages-snippets.js');
        $this->addCss('/plugins/shahin/pages/assets/css/pages.css');

        // Preload the code editor class as it could be needed
        // before it loads dynamically.
        $this->addJs('/modules/backend/formwidgets/codeeditor/assets/js/build-min.js', 'core');

        $this->bodyClass = 'compact-container side-panel-not-fixed';
        $this->pageTitle = 'shahin.pages::lang.plugin.name';
        $this->pageTitleTemplate = '%s Pages';

        if (Request::ajax() && Request::input('formWidgetAlias')) {
            $this->bindFormWidgetToController();
        }
    }

    public function index_onOpen()
    {
        $this->validateRequestTheme();

        $type = Request::input('type');
        $object = $this->loadObject($type, Request::input('path'));

        return $this->pushObjectForm($type, $object);
    }

    public function onSave()
    {
        $this->validateRequestTheme();
        $type = Request::input('objectType');

        $object = $this->fillObjectFromPost($type);

        $object->save();

        $result = [
            'objectPath'  => $type != 'content' ? $object->getBaseFileName() : $object->fileName,
            'objectMtime' => $object->mtime,
            'tabTitle'    => $this->getTabTitle($type, $object)
        ];

     

        $successMessages = [
            'menu' => 'Menu saved successfully.'
        ];

        $successMessage = isset($successMessages[$type])
            ? $successMessages[$type]
            : $successMessages['page'];

        Flash::success(Lang::get($successMessage));

        return $result;
    }

    public function onCreateObject()
    {
        $this->validateRequestTheme();

        $type = Request::input('type');
        $object = $this->createObject($type);
        $parent = Request::input('parent');
        $parentPage = null;

      

        $widget = $this->makeObjectFormWidget($type, $object);
        $this->vars['objectPath'] = '';

        $result = [
            'tabTitle' => $this->getTabTitle($type, $object),
            'tab' => $this->makePartial('form_page', [
                'form'         => $widget,
                'objectType'   => $type,
                'objectTheme'  => $this->theme->getDirName(),
                'objectMtime'  => null,
                'objectParent' => $parent,
                'parentPage'   => $parentPage
            ])
        ];

        return $result;
    }

    public function onDelete()
    {
        $this->validateRequestTheme();

        $type = Request::input('objectType');

        $deletedObjects = $this->loadObject($type, trim(Request::input('objectPath')))->delete();

        $result = [
            'deletedObjects' => $deletedObjects,
            'theme' => $this->theme->getDirName()
        ];

        return $result;
    }

    public function onDeleteObjects()
    {
        $this->validateRequestTheme();

        $type = Request::input('type');
        $objects = Request::input('object');
        if (!$objects) {
            $objects = Request::input('template');
        }

        $error = null;
        $deleted = [];

        try {
            foreach ($objects as $path => $selected) {
                if (!$selected) continue;
                $object = $this->loadObject($type, $path, true);
                if (!$object) continue;

                $deletedObjects = $object->delete();
                if (is_array($deletedObjects)) {
                    $deleted = array_merge($deleted, $deletedObjects);
                }
                else {
                    $deleted[] = $path;
                }
            }
        }
        catch (Exception $ex) {
            $error = $ex->getMessage();
        }

        return [
            'deleted' => $deleted,
            'error'   => $error,
            'theme'   => Request::input('theme')
        ];
    }

 

    public function onGetMenuItemTypeInfo()
    {
        $type = Request::input('type');

        return [
            'menuItemTypeInfo' => MenuItem::getTypeInfo($type)
        ];
    }

    public function onUpdatePageLayout()
    {
        $this->validateRequestTheme();
        $type = Request::input('objectType');

        $object = $this->fillObjectFromPost($type);

        return $this->pushObjectForm($type, $object);
    }

    


    //
    // Methods for the internal use
    //

    protected function validateRequestTheme()
    {
        if ($this->theme->getDirName() != Request::input('theme'))
            throw new ApplicationException(trans('cms::lang.theme.edit.not_match'));
    }

    protected function loadObject($type, $path, $ignoreNotFound = false)
    {
        $class = $this->resolveTypeClassName($type);
        if (!($object = call_user_func(array($class, 'load'), $this->theme, $path))) {
            if (!$ignoreNotFound) {
                throw new ApplicationException(trans('Object not found'));
            }

            return null;
        }

        if ($type == 'content') {
            $fileName = $object->getFileName();
            $extension = pathinfo($fileName, PATHINFO_EXTENSION);

            if ($extension == 'htm') {
                $object->markup_html = $object->markup;
            }
        }

        return $object;
    }

    protected function createObject($type)
    {
        $class = $this->resolveTypeClassName($type);

        if (!($object = new $class($this->theme))) {
            throw new ApplicationException(trans('object not found'));
        }

        return $object;
    }

    protected function resolveTypeClassName($type)
    {
        $types = [
            'menu' => 'Shahin\Pages\Classes\Menu'
        ];

        if (!array_key_exists($type, $types)) {
            throw new ApplicationException(trans('invalid type'));
        }

        return $types[$type];
    }

    protected function makeObjectFormWidget($type, $object, $alias = null)
    {
        $formConfigs = [
            'menu' => '~/plugins/shahin/pages/classes/menu/fields.yaml',
            ];

        if (!array_key_exists($type, $formConfigs)) {
            throw new ApplicationException(trans('object not found'));
        }

        $widgetConfig = $this->makeConfig($formConfigs[$type]);
        $widgetConfig->model = $object;
        $widgetConfig->alias = $alias ?: 'form'.studly_case($type).md5($object->getFileName()).uniqid();

        $widget = $this->makeWidget('Backend\Widgets\Form', $widgetConfig);

       

        return $widget;
    }

 
   

    protected function getTabTitle($type, $object)
    {
      
        if ($type == 'menu') {
            $result = $object->name;
            if (!strlen($result)) {
                $result = trans('New Menu');
            }

            return $result;
        }
      

        return $object->getFileName();
    }

    protected function formatSettings()
    {
        $settings = [];

        if (!array_key_exists('viewBag', $_POST))
            return $settings;

        $settings['viewBag'] = $_POST['viewBag'];

        return $settings;
    }

 

    protected function fillObjectFromPost($type)
    {
        $objectPath = trim(Request::input('objectPath'));
        $object = $objectPath ? $this->loadObject($type, $objectPath) : $this->createObject($type);

        $settings = $this->formatSettings($this->formatSettings());

        $objectData = [];
        if ($settings) {
            $objectData['settings'] = $settings;
        }

        $fields = ['markup', 'code', 'fileName', 'content', 'itemData', 'name'];

        if ($type != 'menu' && $type != 'content') {
            $fields[] = 'parentFileName';
        }

        foreach ($fields as $field) {
            if (array_key_exists($field, $_POST)) {
                $objectData[$field] = Request::input($field);
            }
        }

    

        if (!empty($objectData['markup']) && Config::get('cms.convertLineEndings', false) === true) {
            $objectData['markup'] = $this->convertLineEndings($objectData['markup']);
        }

        if (!Request::input('objectForceSave') && $object->mtime) {
            if (Request::input('objectMtime') != $object->mtime) {
                throw new ApplicationException('mtime-mismatch');
            }
        }

       

        $object->fill($objectData);
        return $object;
    }

    protected function pushObjectForm($type, $object)
    {
        $widget = $this->makeObjectFormWidget($type, $object);

        $this->vars['objectPath'] = Request::input('path');

        if ($type == 'page') {
            $this->vars['pageUrl'] = URL::to($object->getViewBag()->property('url'));
        }

        return [
            'tabTitle' => $this->getTabTitle($type, $object),
            'tab' => $this->makePartial('form_page', [
                'form'        => $widget,
                'objectType'  => $type,
                'objectTheme' => $this->theme->getDirName(),
                'objectMtime' => $object->mtime
            ])
        ];
    }

    protected function bindFormWidgetToController()
    {
        $alias = Request::input('formWidgetAlias');
        $type = Request::input('objectType');
        $object = $this->loadObject($type, Request::input('objectPath'));

        $widget = $this->makeObjectFormWidget($type, $object, $alias);
        $widget->bindToController();
    }

    /**
     * Replaces Windows style (/r/n) line endings with unix style (/n)
     * line endings.
     * @param string $markup The markup to convert to unix style endings
     * @return string
     */
    protected function convertLineEndings($markup)
    {
        $markup = str_replace("\r\n", "\n", $markup);
        $markup = str_replace("\r", "\n", $markup);
        return $markup;
    }
}
