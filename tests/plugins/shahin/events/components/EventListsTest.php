<?php

namespace Shahin\Events\Components;
use Shahin\Events\Components\EventLists;
use Cms\Classes\Page;
use Redirect;
/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-08-21 at 10:06:34.
 */
class EventListsTest extends \TestCase {
    
    /**
     * @covers Shahin\Events\Components\EventLists::componentDetails
     * @todo   Implement testComponentDetails().
     */
    public function testComponentDetails() {
       $eventList = new EventLists;
       $this->assertArrayHasKey('name', $eventList->componentDetails());
       $this->assertArrayHasKey('description', $eventList->componentDetails());
    }

    /**
     * @covers Shahin\Events\Components\EventLists::defineProperties
     * @todo   Implement testDefineProperties().
     */
    public function testDefineProperties() {
       $eventList = new EventLists;
       $this->assertArrayHasKey('pageNumber', $eventList->defineProperties());
       $this->assertArrayHasKey('postsPerPage', $eventList->defineProperties());
       $this->assertArrayHasKey('noPostsMessage', $eventList->defineProperties());
       $this->assertArrayHasKey('sortOrder', $eventList->defineProperties());
       $this->assertArrayHasKey('postPage', $eventList->defineProperties());
    }

    /**
     * @covers Shahin\Events\Components\EventLists::getPostPageOptions
     * @todo   Implement testGetPostPageOptions().
     */
    public function testGetPostPageOptions() {
       $eventList = new EventLists;
       $page = Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
       $this->assertTrue($eventList->getPostPageOptions() == $page);
    }

    /**
     * @covers Shahin\Events\Components\EventLists::getSortOrderOptions
     * @todo   Implement testGetSortOrderOptions().
     */
    public function testGetSortOrderOptions() {
        $eventList = new EventLists;
        $this->assertArrayHasKey('created_at asc', $eventList->getSortOrderOptions());
        $this->assertArrayHasKey('created_at desc', $eventList->getSortOrderOptions());
        $this->assertArrayHasKey('updated_at asc', $eventList->getSortOrderOptions());
        $this->assertArrayHasKey('updated_at desc', $eventList->getSortOrderOptions());
    }

    /**
     * @covers Shahin\Events\Components\EventLists::onRun
     * @todo   Implement testOnRun().
     */
    public function testOnRun() {
//        $eventList = new EventLists;
//        $onRun = $eventList->onRun();
        $this->markTestSkipped('This depends on the page size which can be accomodate by the events list');
    }

}
