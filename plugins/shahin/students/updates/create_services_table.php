<?php namespace Shahin\Students\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{

    public function up()
    {
        Schema::create('shahin_students_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('picture');
            $table->text('passport');
            $table->text('college_name');
            $table->text('status');
            $table->text('agent_name');
            $table->text('contact_number');
            $table->text('payment');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('shahin_students_services');
    }

}
