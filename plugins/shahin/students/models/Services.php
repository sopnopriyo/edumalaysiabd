<?php namespace Shahin\Students\Models;

use Model;

/**
 * Services Model
 */
class Services extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'shahin_students_services';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    public $rules = [
      'picture' => 'required|between:3,64',
    ];
    public function listStatuses($keyValue = null, $fieldName = null)
    {
        return ['Payment pending' => 'Payment Pending',
        'Preparing File' => 'Preparing File',
        'EMGS Processing' => 'EMGS Processing',
        '20% Completed' => '20% Completed',
        '30% Completed' => '30% Completed',
        '40% Completed' => '40% Completed',
        '50 % Completed' => '50 % Completed',
        '70 % Completed' => '70 % Completed',
        '90 % Completed' => '90 % Completed',
        '100 % Completed' => '100 % Completed',
        'VAL Collected' => 'VAL Collected',
        'BD Embassy Processing' => 'BD Embassy Processing',
        'Visa Sticker Stamped' => 'Visa Sticker Stamped',
        'Purchase Air Ticket' => 'Purchase Air Ticket',
        'Ready to Fly' => 'Ready to Fly',
        'Airport Pick Up' => 'Airport Pick Up',
        'Passed Medical Check up' => 'Passed Medical Check up',
        'Faild Medical Check up' => 'Faild Medical Check up',
        'Collect Passport' => 'Collect Passport',
        'Successfully Completed' => 'Successfully Completed',
        'Rejected' => 'Rejected'
        
        
        
        ];
    }
    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}